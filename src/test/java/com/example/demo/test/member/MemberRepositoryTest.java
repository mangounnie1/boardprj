package com.example.demo.test.member;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.member.entity.MemberEntity;
import com.example.demo.member.repository.MemberRepository;

@SpringBootTest
public class MemberRepositoryTest {
	
	@Autowired
	MemberRepository repository;
	
//	@Test
//	public void 회원등록() {
//		MemberEntity memberEntity1 = new MemberEntity("master1", "12345", "관리자1","ROLE_ADMIN");
//		repository.save(memberEntity1);
//		MemberEntity memberEntity2 = new MemberEntity("user1", "1234", "유저1","ROLE_USER");
//		repository.save(memberEntity2);
//		MemberEntity memberEntity3 = new MemberEntity("user2", "1234", "유저2","ROLE_USER");
//		repository.save(memberEntity3);
//	}
	@Test
	public void 데이터단건조회() {
		Optional<MemberEntity> result = repository.findById("user1");
		if(result.isPresent()) {
			MemberEntity memberEntity = result.get();
			System.out.println(memberEntity);
		}
	}
	@Test
	public void 데이터전체조회() {
		List<MemberEntity> list = repository.findAll();
		for(MemberEntity memberEntity : list) {
			System.out.println(memberEntity);
		}
	}
	@Test
	private void 데이터수정() {
		Optional<MemberEntity> result = repository.findById("user1");
		MemberEntity memberEntity = result.get();
		memberEntity.setName("유저이름수정");
		repository.save(memberEntity);
	}
	@Test
	public void 데이터단건삭제() {
		repository.deleteById("user1"); //게시글테이블에서 참조하고 있으면 회원을 삭제할 수 없음
	}
	@Test
	public void 데이터전체삭제() {
		repository.deleteAll();
	}

}
