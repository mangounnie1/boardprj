package com.example.demo.test.member;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;

import com.example.demo.member.dto.MemberDTO;
import com.example.demo.member.service.MemberService;

@SpringBootTest
public class MemberServiceTest {
	
	@Autowired
	MemberService service;
	
	@Test
	public void 일번페이지_목록조회() {
		Page<com.example.demo.member.dto.MemberDTO> page = service.getList(1);
		List<MemberDTO> list = page.getContent();
		for(MemberDTO dto : list) {
			System.out.println(dto);
		}
	}
	@Test
	public void 회원등록() {
		boolean isSuccess = service.register(new MemberDTO("user1","1234","유저1",null,null,"ROLE_USER"));
		if(isSuccess) {
			System.out.println("회원이 등록되었습니다.");
		}else {
			System.out.println("중복된 회원 입니다.");
		}
		
	}
	@Test
	public void 상세조회테스트() {
		MemberDTO dto = service.read("user1");
		System.out.println(dto);
	}

}
