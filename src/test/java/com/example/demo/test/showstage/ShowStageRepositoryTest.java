package com.example.demo.test.showstage;

import java.util.List;


import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.showstage.entity.ShowStageEntity;
import com.example.demo.showstage.repository.ShowStageRepository;


@SpringBootTest
public class ShowStageRepositoryTest {

	@Autowired
	ShowStageRepository repository;
	
//	@Test
//	public void 데이터등록() {
//		ShowStageEntity sst1 = new ShowStageEntity(0,"인천실내체육관","인천시 중구 도원동","인터파크","032-883-1375","","", "","", null, null);
//		repository.save(sst1);
//		ShowStageEntity sst2 = new ShowStageEntity(0,"인천문학월드컵경기장","인천시 시설관리공단 문학경기장","인터파크","032-456-2114", null, "", null, null, null, null);
//		repository.save(sst2);
//		ShowStageEntity sst3 = new ShowStageEntity(0,"인천문화예술회관","인천시 남동구 구월동","인터파크","032-427-8401~5", null, "", null, null, null, null);
//		repository.save(sst3);
//	}
	
	@Test
	public void 데이터단건조회() {
		Optional<ShowStageEntity> result = repository.findById(1);
		if(result.isPresent()) {
			ShowStageEntity sst = result.get();
			System.out.println(sst);
		}
	}
	@Test
	public void 데이터전체조회() {
		List<ShowStageEntity> list = repository.findAll();
		for(ShowStageEntity sst : list) {
			System.out.println(sst);
		}
	}
	
	@Test
	public void 데이터수정() {
		Optional<ShowStageEntity> result = repository.findById(1);
		ShowStageEntity sst = result.get();
		sst.setShowstageName("공연장이름이수정되었습니다!");
		repository.save(sst);	
	}
	
	@Test
	public void 데이터삭제() {
		repository.deleteById(1);
	}
	
	@Test
	public void 데이터전체삭제() {
		repository.deleteAll();
	}
}
