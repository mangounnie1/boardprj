package com.example.demo.test.show;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.show.entity.ShowEntity;
import com.example.demo.show.repository.ShowRepository;
import com.example.demo.showstage.entity.ShowStageEntity;
import com.example.demo.showstage.repository.ShowStageRepository;

@SpringBootTest
public class ShowRepositoryTest {
	@Autowired
	ShowRepository repository;
	
	@Autowired
	ShowStageRepository	repository2;
	
	@Test
	public void 등록() {
		ShowStageEntity stageNo1 = ShowStageEntity.builder().no(1).build();
//		ShowStageEntity stage = ShowStageEntity.builder().no(100).build(); // 존재하지 않는 공연장 번호 사용 시
		ShowEntity show1 = new ShowEntity(null, 0, "인천 1월 콘서트", LocalDate.of(2023, 1, 23), LocalDate.of(2023, 1, 25), "공연 시간 정보", "출연진", "1시간", "특이사항 입력");
		repository.save(show1);
		
		ShowStageEntity stageNo2 = ShowStageEntity.builder().no(2).build();		
		ShowEntity show2 = new ShowEntity(stageNo2, 0, "인천 2월 콘서트", LocalDate.of(2023, 2, 23), LocalDate.of(2023, 2, 25), "공연 시간 정보", "출연진", "50분", "특이사항 입력");
		repository.save(show2);
	}
	
	@Test
	public void 복수등록() {
		for (int i = 1; i <= 4; i++) {		
			ShowStageEntity stageNo = ShowStageEntity.builder().no(1).build();
			ShowEntity show = new ShowEntity(stageNo, 0, "인천 " + i + "월 콘서트", LocalDate.of(2023, i, 23), LocalDate.of(2023, i, 25), "공연 시간 정보", "출연진", "1시간", "비고 입력");
			repository.save(show);
		}
		
		for (int i = 5; i <= 8; i++) {		
			ShowStageEntity stageNo = ShowStageEntity.builder().no(2).build();
			ShowEntity show = new ShowEntity(stageNo, 0, "인천 " + i + "월 콘서트", LocalDate.of(2023, i, 23), LocalDate.of(2023, i, 25), "공연 시간 정보", "출연진", "1시간", "비고 입력");
			repository.save(show);
		}
		
		for (int i = 9; i <= 12; i++) {		
			ShowStageEntity stageNo = ShowStageEntity.builder().no(3).build();
			ShowEntity show = new ShowEntity(stageNo, 0, "인천 " + i + "월 콘서트", LocalDate.of(2023, i, 23), LocalDate.of(2023, i, 25), "공연 시간 정보", "출연진", "1시간", "비고 입력");
			repository.save(show);
		}
	}
	
	@Test
	public void 단건조회() {
		Optional<ShowEntity> result = repository.findById(10);
		if (result.isPresent()) {
			ShowEntity show = result.get();
			System.out.println(show);
		}
	}
	
	@Test
	public void 전체조회() {
		List<ShowEntity> list = repository.findAll();
		for (ShowEntity show : list) {
			System.out.println(show);
		}
	}
	
	@Test
	public void 수정() {
		Optional<ShowEntity> result = repository.findById(1);
		if (result.isPresent()) {
			ShowEntity show = result.get();
			show.setEndDate(LocalDate.of(2023, 1, 24));
			repository.save(show);
		}
	}
	
	@Test
	public void 삭제() {
		repository.deleteById(12);
	}
	
	@Test
	public void 전체삭제() {
		repository.deleteAll();
	}
	
	@Test
	public void 실험() {
		List<ShowStageEntity> list = repository2.findAll();
		List<String> stageList = new ArrayList<>();
		for (int i = 0; i < list.size(); i++) {
			stageList.add(list.get(i).getShowstageName());
		}
		
		for (String s : stageList) {
			System.out.println(s);
		}
	}
}
