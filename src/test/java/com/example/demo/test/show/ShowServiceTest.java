package com.example.demo.test.show;

import java.time.LocalDate;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.show.dto.ShowDTO;
import com.example.demo.show.service.ShowService;

@SpringBootTest
public class ShowServiceTest {
	@Autowired
	ShowService service;
	
	@Test
	public void 공연등록() {
		for (int i = 1; i <=3; i++) {
			service.register(new ShowDTO(2, 0, "인천 공연" + i, "인천문학월드컵경기장", "인천시 시설관리공단 문학경기장", LocalDate.of(2023, i, 25), LocalDate.of(2023, i, 25), "공연 시간 정보", "출연진", "1시간", "비고", null, null));
		}
	}
	
	@Test
	public void 공연목록조회() {
		List<ShowDTO> list = service.getList();
		for (ShowDTO dto : list) {
			System.out.println(dto);
		}
	}
	
	@Test
	public void 공연상세조회() {
		ShowDTO dto = service.read(5);
		System.out.println(dto);
	}
	
	@Test
	public void 공연수정() {
		//존재하는 데이터 수정
		ShowDTO dto = new ShowDTO(2, 15, "인천 공연 15", "인천문학월드컵경기장", "인천시 시설관리공단 문학경기장", LocalDate.of(2023, 1, 25), LocalDate.of(2023, 1, 25), "공연 시간 정보", "출연진", "1시간", "수정됨", null, null);
		service.modify(dto);
		
		//존재하지 않는 데이터 수정 - 변화 없음(메소드에서 데이터가 존재할 때만 수정되도록 설정)
//		ShowDTO dto2 = new ShowDTO(20, "인천 공연 15+", LocalDateTime.of(2023, 5, 25, 9, 0, 0), LocalDateTime.of(2023, 5, 25, 22, 0, 0), "추가 수정됨");
//		service.modify(dto2);
	}
	
	@Test
	public void 공연삭제() {
		service.remove(12);
	}
}
