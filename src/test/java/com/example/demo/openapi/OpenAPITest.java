package com.example.demo.openapi;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;

import org.json.JSONArray;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.show.repository.ShowRepository;
import com.example.demo.showstage.dto.ShowStageDTO;
import com.example.demo.showstage.entity.ShowStageEntity;
import com.example.demo.showstage.repository.ShowStageRepository;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
public class OpenAPITest {
	
	String apiKey = "2023040795PPO5TZV91Z8Z4WRRV4UDWD";
	String resultType = "JSON";
	String code = "PLACE001";
	
	@Autowired
	private ShowStageRepository repository;
	
//	@Test //api수정
	public String getShowstage() throws IOException {
		
		StringBuilder urlBuilder = new StringBuilder("https://iq.ifac.or.kr/openAPI/real/search.do");
		
		urlBuilder.append("?" + URLEncoder.encode("svID","UTF-8") + "=" + URLEncoder.encode("facility", "UTF-8"));
		urlBuilder.append("&" + URLEncoder.encode("apiKey","UTF-8") + "=" + apiKey);
		urlBuilder.append("&" + URLEncoder.encode("resultType","UTF-8") + "=" + URLEncoder.encode(resultType, "UTF-8"));
		urlBuilder.append("&" + URLEncoder.encode("pSize","UTF-8") + "=" + URLEncoder.encode("", "UTF-8"));
		urlBuilder.append("&" + URLEncoder.encode("cPage","UTF-8") + "=" + URLEncoder.encode("", "UTF-8"));
		urlBuilder.append("&" + URLEncoder.encode("srh_placeCode","UTF-8") + "=" + URLEncoder.encode("10", "UTF-8"));
		urlBuilder.append("&" + URLEncoder.encode("srh_cate", "UTF-8") + "=" + URLEncoder.encode(code, "UTF-8"));
		URL url = new URL(urlBuilder.toString());
		
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Content-type", "application/json");
		System.out.println("Response code: " + conn.getResponseCode());
		BufferedReader rd;
		if (conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
			rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		} else {
			rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
		}
		
		StringBuilder sb = new StringBuilder();
		String line;
		while ((line = rd.readLine()) != null) {
			sb.append(line);
		}
		rd.close();
		conn.disconnect();
		System.out.println(sb.toString());

		return sb.toString();

	}
	
	@Test
	public void jsonToDto() throws IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false); 
		String showstage = getShowstage();
		
//		JSONArray jsonArr = (JSONArray)showstage;
		
//		List<ResponseResult> arrayList = Arrays.asList(mapper.readValue(showstage, ResponseResult[].class));
		
		ResponseResult[] list = mapper.readValue(showstage, ResponseResult[].class);
		
		Item[] items = list[0].item;
		
		System.out.println(mapper.readValue(showstage, ResponseResult[].class).toString());
		
//		ResponseResult response = null;
//		response = mapper.readValue(showstage, ResponseResult.class);
//		System.out.println(response.toString());
		
//		ResponseResult response = mapper.readValue(showstage,ResponseResult.class);
		
		for(Item result : items) {
			ShowStageEntity entity = ShowStageEntity.builder()
					.address(result.address) //구주소
					.newAddress(result.newaddress) //신주소
					.description(result.description) //시설상세설명
					.showstageName(result.title) //공연장이름
					.site(result.homepage) //홈페이지
					.phoneNumber(result.tel) //전화번호
					.no(result.idx) //공연장등록번호
					.opentime(result.opentime) //운영시간
					.restday(result.restday) //휴무일
					.build();
			
			repository.save(entity);
		}
}
}
