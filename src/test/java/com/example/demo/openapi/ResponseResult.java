package com.example.demo.openapi;

import java.util.List;

import lombok.Setter;
import lombok.ToString;

@Setter
@ToString
public class ResponseResult {
	Item[] item;
	int totalCnt;
	String resultCode;
	String errorMsg;
	String resultMsg;
}
	

@Setter
@ToString
class Item{
	int idx;
	String title;
	String link;
	String gubun;
	String address;
	String newaddress;
	String tel;
	String fax;
	String homepage;
	String year;
	String size;
	String seat;
	String opentime;
	String restday;
	String poster;
	String description;
	String pubDate;
}