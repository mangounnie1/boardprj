package com.example.demo.show.repository;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.show.entity.ShowEntity;

public interface ShowRepository extends JpaRepository<ShowEntity, Integer> {
	//검색
	Page<ShowEntity> findByShowNameContainingOrArtistsContaining(String keyword, String keyword2, Pageable pageable);
}
