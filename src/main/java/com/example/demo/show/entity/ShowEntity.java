package com.example.demo.show.entity;

import java.time.LocalDate;

import com.example.demo.showstage.entity.ShowStageEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShowEntity extends BaseEntity {
//	@Column(nullable = false)
//	private int stageNo; //공연장 번호
	
	@ManyToOne //FK
	@JoinColumn(name = "stageNo")
	private ShowStageEntity stage; //공연장 번호
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int showNo; //공연 번호
		
	@Column(length = 255, nullable = false)
	private String showName; //공연 이름
	
	@Column(nullable = false)
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDate startDate; //공연 시작일
	
	@Column
//	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDate endDate; //공연 종료일
	
	@Column(length = 1000)
	private String timeExplanation; //공연 시간 정보
	
	@Column(length = 1000)
	private String artists; //출연진
	
	@Column(length = 70)
	private String runningTime; //러닝타임
	
	@Column(length = 1000)
	private String explanation; //비고
}
