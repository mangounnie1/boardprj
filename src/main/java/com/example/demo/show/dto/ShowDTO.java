package com.example.demo.show.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShowDTO {
	private int stageNo; //공연장 번호
	private int showNo; //공연 번호
	private String showName; //공연 이름
	private String showstageName; //공연장이름(join)
	private String address; //주소(join)
	private LocalDate startDate; //공연 시작일
	private LocalDate endDate; //공연 종료일
	private String timeExplanation;
	private String artists;
	private String runningTime; //러닝타임
	private String explanation; //비고
	private LocalDateTime regDate; //등록시간
	private LocalDateTime modDate; //수정시간
}
