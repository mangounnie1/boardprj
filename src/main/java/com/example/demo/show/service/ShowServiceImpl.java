package com.example.demo.show.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.show.dto.ShowDTO;
import com.example.demo.show.entity.ShowEntity;
import com.example.demo.show.repository.ShowRepository;
import com.example.demo.showstage.entity.ShowStageEntity;

@Service
public class ShowServiceImpl implements ShowService {
	@Autowired
	private ShowRepository repository;

	@Override //게시판 형태(임시)
	public List<ShowDTO> getList() {
		List<ShowEntity> entityList = repository.findAll();
		List<ShowDTO> dtoList = new ArrayList<>();
		for (ShowEntity entity : entityList) {
			ShowDTO dto = entityToDto(entity);
			dtoList.add(dto);
		}
		return dtoList;
	}
	
	@Override
	public Page<ShowDTO> getPage(int page) {
		int pageNum = (page == 0) ? 0 : page - 1;
		Pageable pageable = (Pageable) PageRequest.of(pageNum, 15, Sort.by("showNo").descending());
		Page<ShowEntity> entityPage = repository.findAll(pageable);
		Page<ShowDTO> dtoPage = entityPage.map(entity -> entityToDto(entity));
		return dtoPage;
	}
	
	@Override
	public int register(ShowDTO dto) {
		ShowEntity entity = dtoToEntity(dto); //컨트롤러에서 전달받은 dto 데이터를 entity로 변환
		System.out.println(entity);
		repository.save(entity);
		return entity.getShowNo();
	}

	@Override
	public ShowDTO read(int no) {
		Optional<ShowEntity> result = repository.findById(no);
		if (result.isPresent()) {
			ShowEntity entity = result.get();
			return entityToDto(entity);
		} else {
			return null;
		}
	}

	@Override
	public void modify(ShowDTO dto) {
		Optional<ShowEntity> result = repository.findById(dto.getShowNo());
		if (result.isPresent()) {
			ShowStageEntity stageEntity = ShowStageEntity.builder().no(dto.getStageNo()).build();

			ShowEntity entity = result.get();
			entity.setShowName(dto.getShowName());
			entity.setStage(stageEntity);
//			entity.getStage().setNo(dto.getStageNo());
//			entity.getStage().setShowstageName(dto.getShowstageName());
//			entity.getStage().setAddress(dto.getAddress());
			entity.setStartDate(dto.getStartDate());
			entity.setEndDate(dto.getEndDate());
			entity.setTimeExplanation(dto.getTimeExplanation());
			entity.setArtists(dto.getArtists());
			entity.setRunningTime(dto.getRunningTime());
			entity.setExplanation(dto.getExplanation());
			repository.save(entity);
		}
	}

	@Override
	public void remove(int no) {
		repository.deleteById(no);
	}
	
	//검색
	@Override
	public Page<ShowDTO> searchList(String searchKeyword, Pageable pageable) {
		Page<ShowEntity> entityPage = repository.findByShowNameContainingOrArtistsContaining(searchKeyword, searchKeyword, pageable);
		Page<ShowDTO>  dtoPage = entityPage.map(entity -> entityToDto(entity));
		return dtoPage;
	}
}