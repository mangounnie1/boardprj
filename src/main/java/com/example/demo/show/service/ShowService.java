package com.example.demo.show.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.demo.show.dto.ShowDTO;
import com.example.demo.show.entity.ShowEntity;
import com.example.demo.showstage.entity.ShowStageEntity;

public interface ShowService {
	//공연 목록(all)
	List<ShowDTO> getList();
	
	//공연 목록(page)
	Page<ShowDTO> getPage(int page);
	
	//공연 등록
	int register(ShowDTO dto);
	
	//공연 정보 상세보기
	ShowDTO read(int no);	
	
	//공연 정보 수정
	void modify (ShowDTO dto);
	
	//공연 삭제
	void remove(int no);
	
	//검색
	Page<ShowDTO> searchList(String searchKeyword, Pageable pageable);

	//dto -> entity
	default ShowEntity dtoToEntity(ShowDTO dto) {
		ShowStageEntity showStage = ShowStageEntity.builder().no(dto.getStageNo()).showstageName(dto.getShowstageName()).address(dto.getAddress()).build();
		
		ShowEntity entity = ShowEntity.builder()
//				.stageNo(dto.getStageNo()) //외래키 지정하지 않았을 때
				.stage(showStage)
				.showNo(dto.getShowNo())
				.showName(dto.getShowName())
				.stage(showStage)
				.stage(showStage)
				.startDate(dto.getStartDate())
				.endDate(dto.getEndDate())
				.timeExplanation(dto.getTimeExplanation())
				.artists(dto.getArtists())
				.runningTime(dto.getRunningTime())
				.explanation(dto.getExplanation())
				.build();
		return entity;
	}
	
	//entity -> dto
	default ShowDTO entityToDto(ShowEntity entity) {
		ShowDTO dto = ShowDTO.builder()
//				.stageNo(entity.getStageNo()) //외래키 지정하지 않았을 때
				.stageNo(entity.getStage().getNo())
				.showNo(entity.getShowNo())
				.showName(entity.getShowName())
				.showstageName(entity.getStage().getShowstageName())
				.address(entity.getStage().getAddress())
				.startDate(entity.getStartDate())
				.endDate(entity.getEndDate())
				.timeExplanation(entity.getTimeExplanation())
				.artists(entity.getArtists())
				.runningTime(entity.getRunningTime())
				.explanation(entity.getExplanation())
				.regDate(entity.getRegDate())
				.modDate(entity.getModDate())
				.build();
		return dto;		
	}
}
