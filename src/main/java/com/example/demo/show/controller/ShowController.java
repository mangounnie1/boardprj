package com.example.demo.show.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.show.dto.ShowDTO;
import com.example.demo.show.service.ShowService;
import com.example.demo.showstage.dto.ShowStageDTO;
import com.example.demo.showstage.service.ShowStageService;

@Controller
@RequestMapping("/show")
public class ShowController {
	@Autowired
	private ShowService showService;
	
	@Autowired
	private ShowStageService showStageService;
	
	//목록 화면(임시)
	@GetMapping("/list")
	//public void list (Model model) {
	public void list (@RequestParam(defaultValue = "0") int page, @PageableDefault(page =0, size = 15, sort = "showNo", direction = Sort.Direction.DESC) Pageable pageable, String searchKeyword, Model model) { //추가
		//List<ShowDTO> list = showService.getList();
		Page<ShowDTO> list = null;
		
		if (searchKeyword == null) {
			list = showService.getPage(page);
		} else {
			list = showService.searchList(searchKeyword, pageable);
		}
		
		int nowPage = list.getPageable().getPageNumber() + 1;
		int startPage = Math.max(nowPage - 4, 1);
		int endPage = Math.min(nowPage + 5, list.getTotalPages());
		
		model.addAttribute("list", list);
		model.addAttribute("nowPage", nowPage);
		model.addAttribute("startPage", startPage);
		model.addAttribute("endPage", endPage);
		
		System.out.println("전체 페이지 수 : " + list.getTotalPages());
		System.out.println("전체 게시물 수 : " + list.getTotalElements());
		System.out.println("현재 페이지 번호: " + (list.getNumber()+1));
		System.out.println("페이지에 표시할 게시물 수 : " + list.getNumberOfElements());
	}
	
	//등록 화면
	@GetMapping("/register")
	public void register(Model model) {
		List<ShowStageDTO> list = showStageService.getAllList();
		model.addAttribute("list", list);
	}
	
	//등록 처리
	@PostMapping("/register")
	public String registerPost(ShowDTO dto) {
		showService.register(dto);
		return "redirect:/show/calendar";
	}
	
	//상세 보기
	@GetMapping("/read")
	public void read(int showNo, @RequestParam(defaultValue = "0") int page, Model model) {
		ShowDTO dto = showService.read(showNo);
		model.addAttribute("dto", dto);
		
		Page<ShowStageDTO> list = showStageService.getList(page);
		model.addAttribute("list", list);
	}
	
	//수정 화면
	@GetMapping("/modify")
	public void modify(int showNo, Model model) {
		List<ShowStageDTO> list = showStageService.getAllList();
		model.addAttribute("list", list);
		
		ShowDTO dto = showService.read(showNo);
		model.addAttribute("dto", dto);
	}
	
	//수정 처리
	@PostMapping("/modify")
	public String modifyPost(ShowDTO dto, RedirectAttributes redirectAttributes) {
		showService.modify(dto);
		redirectAttributes.addAttribute("showNo", dto.getShowNo());
		return "redirect:/show/read";
	}
	
	//삭제 처리
	@PostMapping("/remove")
	public String removePost(int showNo) {
		showService.remove(showNo);
		return "redirect:/show/calendar";
	}
	
	@GetMapping("/calendar")
	public void test(String searchKeyword, Model model) {
		List<ShowDTO> list = showService.getList();
		model.addAttribute("list", list);
	}
	
}
