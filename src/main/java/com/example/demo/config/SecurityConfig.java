package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

/*시큐리티 설정 클래스*/
@Configuration
@EnableWebSecurity
public class SecurityConfig {
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception{
		
		http.formLogin()
		.loginPage("/customlogin")
		.loginProcessingUrl("/login")
		.defaultSuccessUrl("/", true)
		.permitAll();
		
		// 회원가입, 로그인, 메인페이지 아무나 접근가능
		http.authorizeHttpRequests()
		.requestMatchers("/register","/idcheck","/comment/*", "/").permitAll();
		
		// 웹리소스 경로 접근허용, uploadfile 파일 경로 추가
		http.authorizeHttpRequests().requestMatchers("/swagger-ui/*","/assets/*","/css/*","/js/*","/uploadfile/*").permitAll();
		
		// 메뉴별 접근제한
		http.authorizeHttpRequests()
		.requestMatchers("/show/list","/api/v2/**","/health","/swagger-ui/index.html","/v3/**","/swagger").permitAll()
		.requestMatchers("/show/register").hasRole("ADMIN")
		.requestMatchers("/show/read").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/show/modify").hasRole("ADMIN")
		.requestMatchers("/show/remove").hasRole("ADMIN")
		
		.requestMatchers("/showstage/list").permitAll()
		.requestMatchers("/showstage/search").permitAll() //검색기능 추기
		.requestMatchers("/showstage/register").hasRole("ADMIN")
		.requestMatchers("/showstage/read").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/showstage/modify").hasRole("ADMIN")
		.requestMatchers("/showstage/remove").hasRole("ADMIN")		
		
		.requestMatchers("/board/list").permitAll()
		.requestMatchers("/board/register").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/board/read").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/board/modify").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/board/remove").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/board/search").hasAnyRole("ADMIN", "USER")
		
		
		.requestMatchers("/comment/register").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/comment/read").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/comment/modify").hasAnyRole("ADMIN", "USER")
		.requestMatchers("/comment/remove").hasAnyRole("ADMIN", "USER")
		
		.requestMatchers("/member/list").hasRole("ADMIN") //관리자일경우 접근허용
//		.requestMatchers("/member/list").authenticated()
		.requestMatchers("/member/read").hasAnyRole("ADMIN","USER")
//		.requestMatchers("/member/read").authenticated()
		.requestMatchers("/member/modify").authenticated()
		.requestMatchers("/member/remove").hasAnyRole("ADMIN","USER")
		
		.requestMatchers("/show/calendar").permitAll();
		
		
		
//		http.authorizeHttpRequests()
//		.requestMatchers("/member/*").hasRole("ADMIN");
			
		http.formLogin();
		http.csrf().disable();
		http.logout().logoutSuccessUrl("/"); //로그아웃 처리
		return http.build();
	}
	

	
	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}


}
