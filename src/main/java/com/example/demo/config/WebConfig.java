package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {
	
	//이미지파일 패치추가
	@Value("${webpath}")
	String webpath;
	
	//스프링 보안문제로 외부폴더에 바로 접근할 수 없음
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		//폴더와 상대경로 맵핑
		registry.addResourceHandler("/uploadfile/**").addResourceLocations("file:/C:\\Users\\user\\Desktop\\2조 개발프로젝트\\uploadfile\\");
		WebMvcConfigurer.super.addResourceHandlers(registry);
	}

}
