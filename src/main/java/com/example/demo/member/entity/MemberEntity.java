package com.example.demo.member.entity;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.board.entity.Board;
import com.example.demo.reply.entity.Comment;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name="tbl_member")
public class MemberEntity extends BaseEntity {
	
	@Id
	@Column(length = 50)
	private String id;
	
	@Column(length = 255, nullable = false)
	private String password;
	
	@Column(length = 50, nullable = false)
	private String name;
	
	@Column(length = 30, nullable = false)
	private String role;
	
	//회원삭제시 게시물, 댓글 먼저 삭제하기
	@OneToMany(mappedBy = "writer", cascade = CascadeType.REMOVE)
	private List<Board> boards;
	


}
