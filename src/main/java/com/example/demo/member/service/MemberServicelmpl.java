package com.example.demo.member.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.board.entity.Board;
import com.example.demo.board.repository.BoardRepository;
import com.example.demo.member.dto.MemberDTO;
import com.example.demo.member.entity.MemberEntity;
import com.example.demo.member.repository.MemberRepository;
import com.example.demo.reply.entity.Comment;
import com.example.demo.reply.repository.CommentRepository;

import jakarta.transaction.Transactional;

@Service
public class MemberServicelmpl implements MemberService{
	
	@Autowired
	private MemberRepository repository;
	@Autowired
	private BoardRepository boardRepository;
	@Autowired
	private CommentRepository commentRepository;
	
	@Override
	public Page<MemberDTO> getList(int page) {
		int pageNum = (page == 0) ? 0 : page - 1;
		Pageable pageable = PageRequest.of(pageNum, 10, Sort.by("regDate").descending());
		Page<MemberEntity> entityPage = repository.findAll(pageable);
		Page<MemberDTO> dtoPage = entityPage.map(entity -> entityToDto(entity));
		
		return dtoPage;
	}

	@Override
	public boolean register(MemberDTO dto) {
		String id = dto.getId(); //아이디 중복 체크 
		MemberDTO getDto = read(id); //상세조회
		if (getDto != null) {
			System.out.println("사용중인 아이디입니다.");
			return false;
		}
		MemberEntity entity = dtoToEntity(dto);
		//패스워드 인코더로 패스워드 암호화
		PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String hashpassword = passwordEncoder.encode(entity.getPassword());
		entity.setPassword(hashpassword);
		
		repository.save(entity);
		return true;
	}

	@Override
	public MemberDTO read(String id) {
		Optional<MemberEntity> result = repository.findById(id);
		if (result.isPresent()) {
			MemberEntity member = result.get();
			return entityToDto(member);
		} else {
			return null;
		}
	}
	
	@Override  //회원정보 수정 추가
	public void modify(MemberDTO dto) {
		Optional<MemberEntity> result = repository.findById(dto.getId());
		if(result.isPresent()) {
			MemberEntity entity = result.get();
			entity.setId(dto.getId());
			entity.setName(dto.getName());
			entity.setPassword(dto.getPassword());
			entity.setRole(dto.getRole());
			repository.save(entity);
		}
	}
	
	
	
	// 회원삭제시 게시물, 댓글 삭제 
	
	@Override
    public void remove(String id) {
        MemberEntity member = repository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid member ID: " + id));
        // 해당 회원이 작성한 게시물 삭제
        List<Board> boards = boardRepository.findAllByWriter(member);
        boardRepository.deleteAll(boards);

        // 해당 회원이 작성한 댓글 삭제
        List<Comment> comments = commentRepository.findAllByWriter(member);
        commentRepository.deleteAll(comments);

        repository.delete(member);
    }

//	@Override  //회원정보 삭제 추가
//	public void remove(String id) {
//		repository.deleteById(id);
//	}

	
}
