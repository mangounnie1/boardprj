package com.example.demo.member.service;

import org.springframework.data.domain.Page;

import com.example.demo.member.dto.MemberDTO;
import com.example.demo.member.entity.MemberEntity;

public interface MemberService {
	
	Page<MemberDTO> getList(int pageNumber);
	
	boolean register(MemberDTO dto);
	
	MemberDTO read(String id);
	
	void modify(MemberDTO dto); //수정 추가
	
	void remove (String id); //삭제 추가 /* 회원삭제****
	

	
	
	default MemberDTO entityToDto(MemberEntity entity) {
		MemberDTO dto = MemberDTO.builder()
				.id(entity.getId())
				.password(entity.getPassword())
				.name(entity.getName())
				.regDate(entity.getRegDate())
				.modDate(entity.getModDate())
				.role(entity.getRole())
				.build();
		
		return dto;
	}
	
	default MemberEntity dtoToEntity(MemberDTO dto) {
		MemberEntity entity = MemberEntity.builder()
				.id(dto.getId())
				.password(dto.getPassword())
				.name(dto.getName())
				.role(dto.getRole())
				.build();
		
		return entity;
		}

}
