package com.example.demo.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.member.entity.MemberEntity;

public interface MemberRepository extends JpaRepository<MemberEntity, String>{

}
