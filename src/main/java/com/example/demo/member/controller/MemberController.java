package com.example.demo.member.controller;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.member.dto.MemberDTO;
import com.example.demo.member.service.MemberService;

@Controller
public class MemberController {
	
	@Autowired
	private MemberService service;
	
	@GetMapping("/member/list")
	public void list(@RequestParam(defaultValue = "0") int page, Model model) { //파라미터 추가
		Page<MemberDTO> list = service.getList(page);
		model.addAttribute("list", list);	
	}

	@GetMapping("/register")
	public String register() {
		return "member/register";
	}
	
	@PostMapping("/register")
	public String registerPost(MemberDTO dto, RedirectAttributes redirectAttributes) {
		boolean isSuccess = service.register(dto);
		if(isSuccess) {
			return "redirect:/member/list"; //등록성공시 목록화면으로 가기
		}else {
			redirectAttributes.addFlashAttribute("msg", "아이디가 중복되어 등록에 실패하였습니다");
			return "redirect:/member/register"; //등록실패시 회원가입폼으로 돌아가기
		}
	}

	@GetMapping("/member/read")
	public void read(String id, @RequestParam(defaultValue = "0") int page, Model model) { //파라미터 추가
		MemberDTO dto = service.read(id);
		model.addAttribute("dto", dto);
		model.addAttribute("page", page);
	}
	
	@GetMapping("/member/modify") //회원 수정 추가
	public void modify(String id, Model model) {
		MemberDTO dto = service.read(id);
		model.addAttribute("dto", dto);
	}
	
	@PostMapping("/member/modify") //회원 수정 추가
	public String modifyPost(MemberDTO dto, RedirectAttributes redirectAttributes) {
		service.modify(dto);
		redirectAttributes.addAttribute("id", dto.getId());
		return "redirect:/member/read";
	}
	
	@PostMapping("/member/remove") //회원 삭제 추가
	public String removePost(String id) {
		service.remove(id);
		return "redirect:/logout";
	}
	
	
	@GetMapping("/idcheck")
	public @ResponseBody HashMap<String, Boolean> idCheck(String id){
		HashMap<String , Boolean> result = new HashMap<String,Boolean>();
		MemberDTO memberDTO = service.read(id);
		if(memberDTO != null) {
			result.put("isDuplicate", true);
		}else {
			result.put("isDuplicate", false);
		}
		return result;
	}
	
}
