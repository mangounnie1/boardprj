package com.example.demo.member.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.demo.show.dto.ShowDTO;
import com.example.demo.show.service.ShowService;

@Controller
public class HomeController {

	@Autowired
	private ShowService service;
	
	@GetMapping("/")
	public String home(Model model) {
		List<ShowDTO> list = service.getList();
		model.addAttribute("list", list);
		return "show/calendar";
	}

	@GetMapping("/customlogin")
	public String customLogin() {
		return "home/login";
	}
	
}
