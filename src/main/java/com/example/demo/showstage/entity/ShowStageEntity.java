package com.example.demo.showstage.entity;

import com.example.demo.member.entity.MemberEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder
@Table(name="tbl_showstage")
public class ShowStageEntity extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int no; //공연장번호
	
	@Column(length = 200, nullable = false)
	private String showstageName; //공연장이름
	
	@Column(length = 255, nullable = false)
	private String address; //주소
	
	@Column(length = 255, nullable = true)
	private String newAddress; //신주소
	
	@Column(length = 150, nullable = true)
	private String site; //사이트
	
	@Column(length = 150, nullable = true)
	private String phoneNumber; //전화번호
	
	@Column(length = 100, nullable = true)
	private String restday; //휴무일
	
	@Column(length = 200, nullable = true)
	private String opentime; //운영시간
	
	@Column(length = 3000, nullable = true)
	private String description; //시설 상세설명
	
	//이미지 파일 추가
	@ManyToOne
	private MemberEntity writer;
	
	private String imgPath; //이미지파일 이름
	
	
}
