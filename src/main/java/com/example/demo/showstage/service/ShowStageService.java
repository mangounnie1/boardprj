package com.example.demo.showstage.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.example.demo.showstage.dto.ShowStageDTO;
import com.example.demo.showstage.entity.ShowStageEntity;

public interface ShowStageService {
	
	int register(ShowStageDTO dto);
	
	//목록조회 메소드 변경 (파라미터, 리턴타입)
	Page<ShowStageDTO> getList(int PageNumber);

	ShowStageDTO read(int no);

	void modify(ShowStageDTO dto);

	void remove(int no);
	
	List<ShowStageDTO> getAllList();
	
	Page<ShowStageDTO> search(String keyword, int page); //검색기능추가

	default ShowStageEntity dtoToEntity(ShowStageDTO dto) {
		
		ShowStageEntity entity = ShowStageEntity.builder()
				.no(dto.getNo())
				.showstageName(dto.getShowstageName())
				.address(dto.getAddress())
				.site(dto.getSite())
				.phoneNumber(dto.getPhoneNumber())
				.build();
		return entity;
	}

	default ShowStageDTO entityToDto(ShowStageEntity entity) {
		
		ShowStageDTO dto = ShowStageDTO.builder()
				.no(entity.getNo())
				.showstageName(entity.getShowstageName())
				.address(entity.getAddress())
				.site(entity.getSite())
				.phoneNumber(entity.getPhoneNumber())
				.regDate(entity.getRegDate())
				.modDate(entity.getModDate())
				.newAddress(entity.getNewAddress())
				.restday(entity.getRestday())
				.opentime(entity.getOpentime())
				.description(entity.getDescription())
				.imgPath(entity.getImgPath()) //이미지경로 추가
				.build();
		return dto;
	}

}
