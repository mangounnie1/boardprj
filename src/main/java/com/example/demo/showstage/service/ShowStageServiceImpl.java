package com.example.demo.showstage.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.showstage.dto.ShowStageDTO;
import com.example.demo.showstage.entity.ShowStageEntity;
import com.example.demo.showstage.repository.ShowStageRepository;
import com.example.demo.util.FileUtil;

@Service
public class ShowStageServiceImpl implements ShowStageService {

	@Autowired
	private ShowStageRepository repository;
	
	@Autowired
	private FileUtil fileUtil;

	@Override //이미지파일 추가
	public int register(ShowStageDTO dto) {
		ShowStageEntity entity = dtoToEntity(dto);
		//유틸클래스를 이용해서 파일을 폴더에 저장하고 파일이름을 반환받는다
		String imgPath = fileUtil.fileUpload(dto.getUploadfile());
		//그리고 엔티티에 파일이름을 저장한다
		entity.setImgPath(imgPath);

		System.out.println("DTO----------");
		System.out.println(dto);

//		ShowStageEntity entity = dtoToEntity(dto);
//		System.out.println(entity);
		repository.save(entity);

		return entity.getNo();
	}

	// 목록조회 메소드 변경
	@Override
	public Page<ShowStageDTO> getList(int page) {
		int pageNum = (page == 0) ? 0 : page - 1; // page는 index 처럼 0부터 시작
		Pageable pageable = PageRequest.of(pageNum, 10, Sort.by("no").descending());
		Page<ShowStageEntity> entityPage = repository.findAll(pageable);
		Page<ShowStageDTO> dtoPage = entityPage.map( entity -> entityToDto(entity) );

		return dtoPage;
	}

	@Override
	public ShowStageDTO read(int no) {
		Optional<ShowStageEntity> result = repository.findById(no);
		if (result.isPresent()) {
			ShowStageEntity sstEntity = result.get();
			return entityToDto(sstEntity);
		} else {
			return null;
		}
	}

	@Override
	public void modify(ShowStageDTO dto) {
		Optional<ShowStageEntity> result = repository.findById(dto.getNo());
		ShowStageEntity entity = result.get();
		if (result.isPresent()) {
			//파일스트림이 들어왔을 경우
			//파일 유틸로 파일 save하고 entity의 이미지 경로 변경 -> save()
			//안들어왔을 경우에는
			//기존이미지경로 유지

			if(dto.getUploadfile() != null) {
				
				String imgPath = fileUtil.fileUpload(dto.getUploadfile());
				entity.setImgPath(imgPath);
			}
			
			
			entity.setShowstageName(dto.getShowstageName());
			entity.setAddress(dto.getAddress());
			entity.setSite(dto.getSite());
			entity.setPhoneNumber(dto.getPhoneNumber());
			repository.save(entity);
		}
	}

	@Override
	public void remove(int no) {
		repository.deleteById(no);
	}

	@Override
	public List<ShowStageDTO> getAllList() {
		List<ShowStageEntity> entityList = repository.findAll();
		List<ShowStageDTO> dtoList = new ArrayList<>();
		for (ShowStageEntity entity : entityList) {
			ShowStageDTO dto = entityToDto(entity);
			dtoList.add(dto);
		}
		return dtoList;
	}
	
	//검색기능 추가
	public Page<ShowStageDTO> search(String keyword, int page) {
	    int pageNum = (page == 0) ? 0 : page - 1;
	    Pageable pageable = PageRequest.of(pageNum, 10, Sort.by("no").descending());
	    Page<ShowStageEntity> entityPage = repository.findByShowstageNameContainingOrAddressContaining(keyword, keyword, pageable);
	    Page<ShowStageDTO> dtoPage = entityPage.map(entity -> entityToDto(entity));
	    return dtoPage;
	}

}
