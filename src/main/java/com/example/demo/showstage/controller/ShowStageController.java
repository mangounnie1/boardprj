package com.example.demo.showstage.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.showstage.dto.ShowStageDTO;
import com.example.demo.showstage.service.ShowStageService;

@Controller
@RequestMapping("/showstage")
public class ShowStageController {

	@Autowired
	private ShowStageService service;
	
	/* 목록화면 메소드 수정 */
	@GetMapping("/list")
	public void list(@RequestParam(defaultValue = "0") int page, Model model) { //파라미터 추가
		Page<ShowStageDTO> list = service.getList(page);
		model.addAttribute("list", list);	
		System.out.println("전체 페이지 수: " + list.getTotalPages());
		System.out.println("전체 게시물 수: " + list.getTotalElements());
		System.out.println("현재 페이지 번호: " + (list.getNumber() + 1));
		System.out.println("페이지에 표시할 게시물 수: " + list.getNumberOfElements());
	}

	@GetMapping("/register")
	public void register() {
	}

	@PostMapping("/register")
	public String registerPost(ShowStageDTO dto, RedirectAttributes redirectAttributes) {
		int no = service.register(dto);
		redirectAttributes.addFlashAttribute("msg", no);
		return "redirect:/showstage/list";
	}

	/* 상세화면 메소드 수정 */
	@GetMapping("/read")
	public void read(int no, @RequestParam(defaultValue = "0") int page, Model model) { //파라미터 추가
		ShowStageDTO dto = service.read(no);
		model.addAttribute("dto", dto);
		model.addAttribute("page", page);
	}

	@GetMapping("/modify")
	public void modify(int no, Model model) {
		ShowStageDTO dto = service.read(no);
		model.addAttribute("dto", dto);
	}

	@PostMapping("/modify")
	public String modifyPost(ShowStageDTO dto, RedirectAttributes redirectAttributes) {
		service.modify(dto);
		redirectAttributes.addAttribute("no", dto.getNo());
		return "redirect:/showstage/read";
	}

	@PostMapping("/remove")
	public String removePost(int no) {
		service.remove(no);
		return "redirect:/showstage/list";
	}
	
	//검색기능 추가
	@GetMapping("/search")
	public ModelAndView search(@RequestParam("keyword") String keyword, @RequestParam(defaultValue = "1") int page) {
	    ModelAndView mv = new ModelAndView("/showstage/list");
	    Page<ShowStageDTO> dtoPage = service.search(keyword, page);
	    mv.addObject("list", dtoPage);
	    return mv;
	}

}
