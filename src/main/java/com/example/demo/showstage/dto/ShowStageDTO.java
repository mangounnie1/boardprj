package com.example.demo.showstage.dto;

import java.time.LocalDateTime;

import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Column;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ShowStageDTO {
	
	@Schema(description = "공연장번호")
	private int no; //공연장번호
	@Schema(description = "공연장이름")
	private String showstageName; //공연장이름
	@Schema(description = "주소")
	private String address; //주소
	@Schema(description = "신주소")
	private String newAddress; //신주소
	@Schema(description = "사이트")
	private String site; //사이트
	@Schema(description = "전화번호")
	private String phoneNumber; //전화번호
	@Schema(description = "휴무일")
	private String restday; //휴무일
	@Schema(description = "운영시간")
	private String opentime; //운영시간
	@Schema(description = "시설 상세설명")
	private String description; //시설 상세설명
	@Schema(description = "등록일")
	private LocalDateTime regDate; //등록일
	@Schema(description = "수정일")
	private LocalDateTime modDate; //수정일
	//파일 업로드 추가
	private MultipartFile uploadfile; //파일 스트림
	private String imgPath; //파일 이름

}
