package com.example.demo.showstage.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.showstage.entity.ShowStageEntity;

public interface ShowStageRepository extends JpaRepository<ShowStageEntity,Integer>{

	//검색기능 추가
	Page<ShowStageEntity> findByShowstageNameContainingOrAddressContaining(String keyword, String keyword2,
			Pageable pageable);

}
