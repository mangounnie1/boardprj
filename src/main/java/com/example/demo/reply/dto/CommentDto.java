package com.example.demo.reply.dto;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.demo.member.entity.MemberEntity;
import com.example.demo.reply.entity.Comment;
import com.example.demo.reply.repository.CommentRepository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class CommentDto {


	private int commentNo;
	private int boardNo;
	private String content;
	private String writer;
	private LocalDateTime regDate;
	private LocalDateTime updateDate;

	

}
