package com.example.demo.reply.controller;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.demo.reply.dto.CommentDto;
import com.example.demo.reply.entity.Comment;
import com.example.demo.reply.exception.UnauthorizedException;
import com.example.demo.reply.service.CommentService;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import jakarta.servlet.http.HttpSession;

@Controller
@RequestMapping("/comment")
public class CommentController {
	@Autowired
	CommentService service;
	//게시물별 댓글 목록 조회
		@ResponseBody
		@GetMapping("/list")
		public HashMap<String,List<CommentDto>> list(int boardNo, Model model) {
			HashMap<String,List<CommentDto>> map = new HashMap<>();
			List<CommentDto> commentlist = service.getListByBoardNo(boardNo);
			map.put("commentlist", commentlist);
			
			return map;
		}
		
		@ResponseBody
		@PostMapping("/register")
		public HashMap<String,Boolean> register(CommentDto dto, Principal principal) {
			HashMap<String,Boolean> map = new HashMap<>();
			String id = principal.getName();
			dto.setWriter(id);		
			service.register(dto);
			map.put("success", true);
			return map;
		}
		
	
		/*@ResponseBody
		@PostMapping("/modify")
		public HashMap<String, Object> modify(CommentDto dto, Principal principal) {
		    HashMap<String, Object> map = new HashMap<>();
		    try {
		        String id = principal.getName();
		        CommentDto commentDto = service.read(dto.getCommentNo());
		        if (!commentDto.getWriter().equals(id)) { // 작성자와 로그인한 사용자의 아이디 비교
		            // 작성자와 로그인한 사용자가 다른 경우, 수정을 허용하지 않고 메시지 반환
		            map.put("success", false);
		            map.put("message", "본인이 작성한 댓글이 아니므로 수정할 수 없습니다.");
		            return map;
		        } else {
		            dto.setWriter(id);
		            service.modify(dto);
		            map.put("success", true);
		            return map;
		        }
		    } catch (NullPointerException e) {
		        map.put("success", false);
		        map.put("message", "댓글 수정이 완료되지 않았습니다.");
		        return map;
		    }
		}*/
		

	
		@PostMapping("/modify")
		@ResponseBody
		public Map<String, Boolean> modify(int commentNo, String content, Principal principal) {
		    Map<String, Boolean> map = new HashMap<>();

		    try {
		        service.modify(commentNo, content, principal.getName());
		        map.put("success", true);
		    } catch (UnauthorizedException e) {
		        map.put("success", false);
		        map.put("error", Boolean.FALSE);
		    } catch (IllegalArgumentException e) {
		        map.put("success", false);
		        map.put("error",!e.getMessage().isEmpty());
		    }

		    return map;
		}
		
		
		 @PostMapping("/remove")
		    @ResponseBody
		    public Map<String, Boolean> remove(int commentNo, Principal principal) {
		        Map<String, Boolean> map = new HashMap<>();

		        try {
		            service.remove(commentNo, principal.getName());
		            map.put("success", true);
		        } catch (UnauthorizedException e) {
		            map.put("success", false);
		            map.put("error", Boolean.FALSE);
		        } catch (IllegalArgumentException e) {
		            map.put("success", false);
		            map.put("error", !e.getMessage().isEmpty());
		        }

		        return map;
		    }
		
}
