package com.example.demo.reply.service;

import java.security.Principal;
import java.util.List;

import org.springframework.security.core.Authentication;

import com.example.demo.board.entity.Board;
import com.example.demo.member.entity.MemberEntity;
import com.example.demo.reply.dto.CommentDto;
import com.example.demo.reply.entity.Comment;
import com.example.demo.reply.exception.UnauthorizedException;

public interface CommentService {
	
	int register(CommentDto dto);
	List<CommentDto> getList();
	List<CommentDto> getListByBoardNo(int boardNo);
	CommentDto read(int no);
	//댓글 수정 
	void modify(int commentNo, String content, String username) throws UnauthorizedException, IllegalArgumentException;
	//댓글 삭제
	void remove(int commentNo, String username) throws UnauthorizedException;
	
	default Comment dtoToEntity(CommentDto dto) {
		MemberEntity member = MemberEntity.builder().id(dto.getWriter()).build();
		Board board = Board.builder().no(dto.getBoardNo()).build();
		Comment entity = Comment.builder()
				.commentNo(dto.getCommentNo())
				.board(board)
				.content(dto.getContent())
				.writer(member)
				.build();
		return entity;
	}
	
	default CommentDto entityToDto(Comment entity) {
		CommentDto dto = CommentDto.builder()
				.commentNo(entity.getCommentNo())
				.boardNo(entity.getBoard().getNo())
				.content(entity.getContent())
				.writer(entity.getWriter().getId())
				.regDate(entity.getRegDate())
				.updateDate(entity.getUpdateDate())
				.build();
		return dto;
	}

}
