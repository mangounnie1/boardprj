package com.example.demo.reply.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.example.demo.board.entity.Board;
import com.example.demo.member.entity.MemberEntity;
import com.example.demo.reply.dto.CommentDto;
import com.example.demo.reply.entity.Comment;
import com.example.demo.reply.exception.UnauthorizedException;
import com.example.demo.reply.repository.CommentRepository;

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	CommentRepository repository;
	
	
	@Override
	public int register(CommentDto dto) {
		Comment entity = dtoToEntity(dto);
		repository.save(entity);
		return entity.getCommentNo();
		
	}

	@Override
	public List<CommentDto> getList() {
		List<Comment> entityList = repository.findAll();
		List<CommentDto> dtoList = new ArrayList<>();
		for (Comment entity : entityList) {
			CommentDto dto = entityToDto(entity);
			dtoList.add(dto);
		}

		return dtoList;
	}

	@Override
	public List<CommentDto> getListByBoardNo(int boardNo) {
		Board board = Board.builder().no(boardNo).build();  //엔티티 생성
		List<Comment> entityList = repository.findByBoard(board);
		List<CommentDto> dtoList = new ArrayList<>();
		for (Comment entity : entityList) {
			CommentDto dto = entityToDto(entity);
			dtoList.add(dto);
		}

		return dtoList;
	}

	@Override
	public CommentDto read(int no) {
		Optional<Comment> result = repository.findById(no);
		if(result.isPresent()) {
			Comment entity = result.get();
			return entityToDto(entity);
		}
		return null;
	}

	
	 @Override
	    public void remove(int commentNo, String username) throws UnauthorizedException {
	        Comment comment = repository.findById(commentNo)
	                .orElseThrow(() -> new IllegalArgumentException("댓글이 존재하지 않습니다."));

	        if (!comment.getWriter().getId().equals(username)) {
	            throw new UnauthorizedException("댓글 작성자가 아닙니다.");
	        }

	        repository.delete(comment);
	    }
	 
	 @Override
	 public void modify(int commentNo, String content, String username) throws UnauthorizedException, IllegalArgumentException {
	     Comment comment = repository.findById(commentNo)
	             .orElseThrow(() -> new IllegalArgumentException("존재하지 않는 댓글입니다."));

	     if (!comment.getWriter().getId().equals(username)) {
	         throw new UnauthorizedException("댓글 작성자가 아닙니다.");
	     }

	     comment.setContent(content);
	     repository.save(comment);
	 }


}
