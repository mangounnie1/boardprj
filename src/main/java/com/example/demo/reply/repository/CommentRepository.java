package com.example.demo.reply.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.board.entity.Board;
import com.example.demo.member.entity.MemberEntity;
import com.example.demo.reply.entity.Comment;

import jakarta.transaction.Transactional;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Integer>{
	List<Comment> findByBoard(Board board);
	
	@Transactional
	void deleteByBoard(Board board);
	
	// 회원삭제시 댓글 삭제
	 List<Comment> findAllByBoard(Board board);
	 List<Comment> findAllByWriter(MemberEntity writer);
	 List<Comment> findAllByCommentNo(int commentNo);
	 List<Comment> findAllByWriterId(String id);
	
}
