package com.example.demo.board.dto;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@ToString
@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor

public class BoardDTO {

	private int no; //게시물 번호
	private String title;  //제목
	private String content; //내용
	private String writer; //작성자(아이디가 필요)
	private LocalDateTime regDate; //등록일
	private LocalDateTime updateDate; // 수정일
	private int hits; // 조회수
	
	
	//게시판 검색기능
	private String keyword;
	private BoardDTO boardDTO;

}
