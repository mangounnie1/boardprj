package com.example.demo.board.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.board.dto.BoardDTO;
import com.example.demo.board.service.BoardService;

import jakarta.persistence.EntityNotFoundException;




@Controller
@RequestMapping("/board")


public class BoardController {
	@Autowired
	private BoardService boardService;
	
	
	
		//목록화면
		@GetMapping("/list")
		public void list(@RequestParam(defaultValue="0") int page, Model model) {
			Page<BoardDTO> list =boardService.getList(page);
			model.addAttribute("list",list);
			System.out.println("전체 페이지 수 : "+list.getTotalPages());
			System.out.println("전체 게시물 수 : "+list.getTotalElements());
			System.out.println("현재 페이지 번호: "+(list.getNumber()+1));
			System.out.println("페이지에 표시할 게시물 수 : "+ list.getNumberOfElements());
		}
	
		@GetMapping("/register")
		public void register() {
		}

		@GetMapping("/read")//, 
		public void read(int no, @RequestParam(defaultValue = "0") int page, Model model) { //파라미터 추가
			boardService.updateHits(no);
			BoardDTO dto = boardService.read(no);
			model.addAttribute("dto", dto);
			model.addAttribute("page", page);
		}

		

		@PostMapping("/register")
		public String registerPost(BoardDTO dto, RedirectAttributes redirectAttributes, Principal principal) {
			String id = principal.getName();
			dto.setWriter(id);
			int no = boardService.register(dto);
			redirectAttributes.addFlashAttribute("msg", no);
			return "redirect:/board/list";
		}

		@GetMapping("/modify")
		public String modify(int no, Model model, Principal principal, RedirectAttributes redirectAttributes) {
		    BoardDTO boardDTO = boardService.read(no);
		    String writer = boardDTO.getWriter();
		    String loginId = principal.getName();
		    if (!writer.equals(loginId)) { // 작성자와 로그인한 사용자의 아이디 비교
		        // 작성자와 로그인한 사용자가 다른 경우, 수정을 허용하지 않고 글 목록으로 이동
		        redirectAttributes.addAttribute("message", "본인이 작성한 글이 아니므로 수정할 수 없습니다.");
		        return "redirect:/board/list";
		    }
		    model.addAttribute("dto", boardDTO);
		    return "board/modify";
		}
		

		@PostMapping("/modify")
		public String modifyPost(BoardDTO dto, RedirectAttributes redirectAttributes, Principal principal) {
		    String id = principal.getName();
		    dto.setWriter(id);
		    BoardDTO boardDTO = boardService.read(dto.getNo());
		    if (!boardDTO.getWriter().equals(id)) { // 작성자와 로그인한 사용자의 아이디 비교
		        // 작성자와 로그인한 사용자가 다른 경우, 수정을 허용하지 않고 글 목록으로 이동
		        redirectAttributes.addAttribute("message", "본인이 작성한 글이 아니므로 수정할 수 없습니다.");
		        return "redirect:/board/list";
		    }
		    boardService.modify(dto);
		    redirectAttributes.addAttribute("no", dto.getNo());
		    return "redirect:/board/read";
		}
		
		
		@PostMapping("/remove")
			public String remove(int no) {
				boardService.remove(no);
				return"redirect:/board/list";
		}
		
		
		
		//검색기능 추가
//		@GetMapping("/search")
//		public ModelAndView search(@RequestParam(value = "keyword") String keyword, @RequestParam(defaultValue = "1") int page) {
//		    ModelAndView mv = new ModelAndView("/board/list");
//		    Page<BoardDTO> dtoPage = boardService.search(keyword, page);
//		    mv.addObject("list", dtoPage);
//		    return mv;
//		}

		@GetMapping("/search")
		public ModelAndView search(@RequestParam(value = "keyword", required = false) String keyword, @RequestParam(defaultValue = "1") int page) {
		    ModelAndView mv = new ModelAndView("/board/list");
		    try {
		        Page<BoardDTO> dtoPage = boardService.search(keyword, page);
		        mv.addObject("list", dtoPage);
		        if (!dtoPage.hasContent()) {
		            mv.addObject("message", "검색 결과가 없습니다.");
		        }
		    } catch (EntityNotFoundException e) {
		        mv.addObject("message", e.getMessage());
		        mv.addObject("list", null); // list가 null인 경우에 대한 처리
		    }
		    return mv;
		}
	
		
}