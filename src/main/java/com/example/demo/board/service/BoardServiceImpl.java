package com.example.demo.board.service;


import java.util.ArrayList;
import java.util.Optional;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.board.dto.BoardDTO;
import com.example.demo.board.entity.Board;
import com.example.demo.board.repository.BoardRepository;
import com.example.demo.showstage.dto.ShowStageDTO;
import com.example.demo.showstage.entity.ShowStageEntity;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;



@Service
@RequiredArgsConstructor

public class BoardServiceImpl implements BoardService {
	
	@Autowired
	private  BoardRepository repository; // 자동주입
	
	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public int register(BoardDTO dto) {
		System.out.println(dto);
		Board entity = dtoToEntity(dto);
		repository.save(entity);
		return entity.getNo();
	}

	@Override
	public Page<BoardDTO> getList(int page) {
		int pageNum = (page ==0) ? 0 : page - 1;
		Pageable pageable = PageRequest.of(pageNum, 10, Sort.by("no").descending());
		Page<Board> boardpage = repository.findAll(pageable);
		Page<BoardDTO> dtopage = boardpage.map( entity -> entityToDto(entity));
		return dtopage; 
	}
	
	@Override
	public BoardDTO read(int no) {
		Optional<Board> result = repository.findById(no);
		if (result.isPresent()) {
			Board board = result.get();
			return entityToDto(board);
		} else {
			return null;
		}
	}

	@Transactional
	public void updateHits(int no) {
		repository.updateHits(no);
	}

	@Override
	public void modify(BoardDTO dto) {
		Optional<Board> result = repository.findById(dto.getNo());
		if(result.isPresent()) {
			Board entity = result.get();
			entity.setTitle(dto.getTitle());
			entity.setContent(dto.getContent());
			repository.save(entity);
		}	
	}
	
	 @Override
	    public void remove(int no) {
	        Board board = repository.findById(no)
	            .orElseThrow(() -> new RuntimeException("게시물을 찾을 수 없습니다."));
	        
	       repository.delete(board);
	    }

	 
	 //검색기능
//	 @Override
//	    public Page<BoardDTO> search(String keyword, int page) {
//	        int pageNum = (page == 0) ? 0 : page - 1;
//	        Pageable pageable = PageRequest.of(pageNum, 10, Sort.by("no").descending());
//	        Page<Board> entityPage = repository.findByTitleContainingOrContentContaining(keyword, keyword, pageable);
//		    Page<BoardDTO> dtoPage = entityPage.map(entity -> entityToDto(entity));
//		    return dtoPage;
//
//	 }
	 public Page<BoardDTO> search(String keyword, int page) {
		    int pageNum = (page == 0) ? 0 : page - 1;
		    Pageable pageable = PageRequest.of(pageNum, 10, Sort.by("no").descending());
		    Page<Board> entityPage = repository.findByTitleContainingOrContentContaining(keyword, keyword, pageable);
		    Page<BoardDTO> dtoPage = entityPage.map(entity -> entityToDto(entity));
		    if (!dtoPage.hasContent()) {
		        throw new EntityNotFoundException("검색 결과가 없습니다.");
		    }
		    return dtoPage;
		}







}

