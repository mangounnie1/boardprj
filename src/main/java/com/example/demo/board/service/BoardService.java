package com.example.demo.board.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;

import com.example.demo.board.dto.BoardDTO;
import com.example.demo.board.entity.Board;
import com.example.demo.member.entity.MemberEntity;
import com.example.demo.member.repository.MemberRepository;
import com.example.demo.showstage.dto.ShowStageDTO;

import jakarta.persistence.EntityNotFoundException;


public interface BoardService {
	
	int register(BoardDTO dto);
	
	//페이징 
	
	Page<BoardDTO> getList(int pageNumber);
	//목록조회
	
	
	//BoardDTO read(String title);
	BoardDTO read(int no);
	void modify(BoardDTO dto);
	void remove(int no);
	void updateHits(int no);
	// 검색 메소드
	Page<BoardDTO> search(String keyword, int page);
	
	
	default Board dtoToEntity(BoardDTO dto) {
		MemberEntity member = MemberEntity.builder().id(dto.getWriter()).build();

	    Board board = Board.builder()
	            .no(dto.getNo())
	            .title(dto.getTitle())
	            .content(dto.getContent())
	            .writer(member)
	            .hits(dto.getHits())
	            .build();
	    return board;
	}

	
	default BoardDTO entityToDto(Board board) {
		BoardDTO boardDTO = BoardDTO.builder()
	        .no(board.getNo())
	        .title(board.getTitle())
	        .content(board.getContent())
	        .hits(board.getHits())
	        .regDate(board.getRegDate())
	        .updateDate(board.getUpdateDate())
	        .writer(board.getWriter().getId())
	        .build();
		return boardDTO;
	}

}

