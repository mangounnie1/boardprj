package com.example.demo.board.entity;

import java.util.ArrayList;
import java.util.List;

import com.example.demo.member.entity.MemberEntity;
import com.example.demo.reply.entity.Comment;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Board extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int no; //게시물 번호
	
	@Column(length = 150, nullable=false)
	private String title; // 제목
	
	@Column(length = 2000, nullable=false)
	private String content; //내용
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "member_id", nullable = true)
	private MemberEntity writer; //멤버 아이디 fk
	
	@Column
	private int hits;
	
	//보드 삭제 
	@OneToMany(mappedBy = "board", cascade = CascadeType.REMOVE)
	private List<Comment> comments;// = new ArrayList<>();
	
	

	
}
