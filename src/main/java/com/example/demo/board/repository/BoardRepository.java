package com.example.demo.board.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.board.entity.Board;
import com.example.demo.member.entity.MemberEntity;
import com.example.demo.showstage.entity.ShowStageEntity;

public interface BoardRepository extends JpaRepository<Board, Integer> {

	@Modifying
	@Query(value = "update Board b set b.hits=b.hits+1 where b.no=:no")
	void updateHits(@Param("no")int no);
	
	// 회원삭제시 게시물 삭제
	List<Board> findAllByWriter(MemberEntity writer);
	
	////검색기능 추가
	Page<Board> findByTitleContainingOrContentContaining(String keyword, String keyword2, Pageable pageable);


	

}














	
	
	
	
	
	
	
	
	
	
